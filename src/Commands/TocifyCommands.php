<?php

namespace Drupal\tocify\Commands;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drush\Commands\DrushCommands;
use Drush\Drush;
use Drush\Exec\ExecTrait;
use Psr\Container\ContainerInterface as DrushContainer;

/**
 * Tocify drush commandfile.
 */
class TocifyCommands extends DrushCommands {

  const LIBRARY_VERSION = '1.9.0';

  const LIBRARY_DOWNLOAD_URL = 'https://github.com/gfranko/jquery.tocify.js/archive/v' . self::LIBRARY_VERSION . '.zip';

  const LIBRARY_DESTINATION = 'libraries';

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\Core\File\FileSystem $file_system
   *   The file system service.
   */
  public function __construct($file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
      ContainerInterface $container,
      DrushContainer $drush
  ): self {
    return new self(
      $container->get('file_system')
    );
  }

  /**
   * Download and extract library.
   *
   * @usage tocify-download-tocify
   *   Download Tocify library
   *
   * @command tocify:download-tocify
   * @aliases tdlt
   */
  public function download() {
    $this->logger()->notice('Downloading library...');
    $this->downloadLibrary(self::LIBRARY_DOWNLOAD_URL, 'jquery.tocify');
  }

  /**
   * Download and extract a library.
   *
   * @param string $url
   *   The URL to download.
   * @param string $destination
   *   The path to copy the files to.
   *
   * @throws \Exception
   */
  public function downloadLibrary($url, $destination) {
    if (!is_dir(self::LIBRARY_DESTINATION)) {
      drush_op('mkdir', self::LIBRARY_DESTINATION);
      $this->logger()->notice('Directory ' . self::LIBRARY_DESTINATION . ' was created.');
    }

    // Set the directory to the download location.
    $olddir = getcwd();
    chdir(self::LIBRARY_DESTINATION);

    // Download the archive.
    $filename = basename($url);
    if ($filepath = $this->downloadFile($url, FALSE, FALSE, $this->fileSystem->getTempDirectory() . '/' . $filename, TRUE)) {
      $filename = basename($url);

      // Remove any existing plugin directory.
      if (is_dir($destination)) {
        $this->fileSystem->deleteRecursive($destination);
      }

      // Decompress the archive.
      $zip = new \ZipArchive();
      if ($zip->open($filepath) === TRUE) {
        $index = $zip->getNameIndex(0);

        $zip->extractTo('.');
        $zip->close();

        $this->fileSystem->move($index, $destination);
        $this->logger()->notice('The library has been downloaded to ' . $destination);
      }
      else {
        throw new \Exception("Cannot extract '$filename', not a valid archive");
      }
    }

    // Set working directory back to the previous working directory.
    chdir($olddir);

    if (is_dir(self::LIBRARY_DESTINATION . '/' . $destination)) {
      $this->logger()->info('The plugin has been installed to ' . self::LIBRARY_DESTINATION . '/' . $destination);
    }
    else {
      $this->logger()->error('Drush was unable to install the plugin to ' . self::LIBRARY_DESTINATION . '/' . $destination);
    }
  }

  /**
   * Downloads a file.
   *
   * Optionally uses user authentication, using either wget or curl, as
   * available.
   *
   * @param string $url
   *   The URL to download.
   * @param string $user
   *   The username for authentication.
   * @param string $password
   *   The password for authentication.
   * @param string $destination
   *   The destination folder to copy the download to.
   * @param bool $overwrite
   *   Whether to overwrite an existing destination folder.
   *
   * @return string
   *   The destination folder.
   *
   * @throws \Exception
   */
  protected function downloadFile($url, $user = '', $password = '', $destination = '', $overwrite = TRUE) {
    static $use_wget;
    if ($use_wget === NULL) {
      $use_wget = ExecTrait::programExists('wget');
    }

    $destination_tmp = drush_tempnam('download_file');
    if ($use_wget) {
      $args = ['wget', '-q', '--timeout=30'];
      if ($user && $password) {
        $args = array_merge($args, [
          "--user=$user",
          "--password=$password",
          '-O',
          $destination_tmp,
          $url,
        ]);
      }
      else {
        $args = array_merge($args, ['-O', $destination_tmp, $url]);
      }
    }
    else {
      $args = ['curl', '-s', '-L', '--connect-timeout 30'];
      if ($user && $password) {
        $args = array_merge($args, [
          '--user',
          "$user:$password",
          '-o',
          $destination_tmp,
          $url,
        ]);
      }
      else {
        $args = array_merge($args, ['-o', $destination_tmp, $url]);
      }
    }
    $process = Drush::process($args);
    $process->mustRun();

    if (!Drush::simulate()) {
      if (!drush_file_not_empty($destination_tmp) && $file = @file_get_contents($url)) {
        @file_put_contents($destination_tmp, $file);
      }
      if (!drush_file_not_empty($destination_tmp)) {
        // Download failed.
        throw new \Exception(dt("The URL !url could not be downloaded.", ['!url' => $url]));
      }
    }
    if ($destination) {
      $this->fileSystem
        ->move($destination_tmp, $destination, $overwrite);
      return $destination;
    }
    return $destination_tmp;
  }

}
