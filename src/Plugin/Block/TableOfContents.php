<?php

namespace Drupal\tocify\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'TableOfContents' block.
 *
 * @Block(
 *  id = "tocify_table_of_contents",
 *  admin_label = @Translation("Table of contents"),
 * )
 */
final class TableOfContents extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config manager definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactory $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $defaults = $this->configFactory
      ->getEditable('tocify.settings');

    $form['theme'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Theme'),
      '#description' => $this->t('Choose the theme, e.g. "bootstrap", "jqueryui" or "none"'),
      '#default_value' => $this->configuration['theme'] ?? $defaults->get('theme'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['container'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Context'),
      '#description' => $this->t('Choose any valid jQuery selector, e.g. "body"'),
      '#default_value' => $this->configuration['container'] ?? $defaults->get('container'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['selectors'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Selectors'),
      '#description' => $this->t('Each comma separated selector must be a header element, e.g. "h1,h2,h3"'),
      '#default_value' => $this->configuration['selectors'] ?? $defaults->get('selectors'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['show_and_hide'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show and hide'),
      '#description' => $this->t('Should elements be shown and hidden'),
      '#default_value' => $this->configuration['show_and_hide'] ?? $defaults->get('show_and_hide'),
      '#weight' => '0',
    ];
    $form['show_effect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Show effect'),
      '#description' => $this->t('Any of the jQuery show effects, e.g. "none", "fadeIn", "show", or "slideDown"'),
      '#default_value' => $this->configuration['show_effect'] ?? $defaults->get('show_effect'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['show_effect_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Show effect speed'),
      '#description' => $this->t('The time duration of the show effect, e.g. "slow", "medium", "fast", or any numeric number (milliseconds)'),
      '#default_value' => $this->configuration['show_effect_speed'] ?? $defaults->get('show_effect_speed'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['hide_effect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hide effect'),
      '#description' => $this->t('Any of the jQuery hide effects, e.g. "none", "fadeOut", "hide" or "slideUp"'),
      '#default_value' => $this->configuration['hide_effect'] ?? $defaults->get('hide_effect'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['hide_effect_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hide effect speed'),
      '#description' => $this->t('The time duration of the hide effect, e.g. "slow", "medium", "fast", or any numeric number (milliseconds)'),
      '#default_value' => $this->configuration['hide_effect_speed'] ?? $defaults->get('hide_effect_speed'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['smooth_scroll'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Smooth scroll'),
      '#description' => $this->t('Animates the page scroll when specific table of content items are clicked and the page moves up or down'),
      '#default_value' => $this->configuration['smooth_scroll'] ?? $defaults->get('smooth_scroll'),
      '#weight' => '0',
    ];
    $form['smooth_scroll_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Smooth scroll speed'),
      '#description' => $this->t('The time duration of the animation'),
      '#default_value' => $this->configuration['smooth_scroll_speed'] ?? $defaults->get('smooth_scroll_speed'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['scroll_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scroll to'),
      '#description' => $this->t('The amount of space between the top of page and the selected table of contents item after the page has been scrolled'),
      '#default_value' => $this->configuration['scroll_to'] ?? $defaults->get('scroll_to'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['show_and_hide_on_scroll'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show and hide on scroll'),
      '#description' => $this->t('Determines if table of content nested items should be shown and hidden while a user scrolls the page'),
      '#default_value' => $this->configuration['show_and_hide_on_scroll'] ?? $defaults->get('show_and_hide_on_scroll'),
      '#weight' => '0',
    ];
    $form['highlight_on_scroll'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Highlight on scroll'),
      '#description' => $this->t('Determines if table of content nested items should be highlighted while scrolling'),
      '#default_value' => $this->configuration['highlight_on_scroll'] ?? $defaults->get('highlight_on_scroll'),
      '#weight' => '0',
    ];
    $form['highlight_offset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Highlight offset'),
      '#description' => $this->t('The offset distance in pixels to trigger the next active table of contents item'),
      '#default_value' => $this->configuration['highlight_offset'] ?? $defaults->get('highlight_offset'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['extend_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Extend page'),
      '#description' => $this->t('If a user scrolls to the bottom of the page and the page is not tall enough to scroll to the last table of contents item, then the page height is increased'),
      '#default_value' => $this->configuration['extend_page'] ?? $defaults->get('extend_page'),
      '#weight' => '0',
    ];
    $form['extend_page_offset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Extend page offset'),
      '#description' => $this->t('How close to the bottom of the page a user must scroll before the page is extended'),
      '#default_value' => $this->configuration['extend_page_offset'] ?? $defaults->get('extend_page_offset'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['history'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('History'),
      '#description' => $this->t('Adds a hash to the page url to maintain history'),
      '#default_value' => $this->configuration['history'] ?? $defaults->get('history'),
      '#weight' => '0',
    ];
    $form['hash_generator'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Hash generator"),
      '#description' => $this->t("How the URL hash value get's generated"),
      '#default_value' => $this->configuration['hash_generator'] ?? $defaults->get('hash_generator'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['highlight_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Highlight default'),
      '#description' => $this->t("Set's the first table of content item as active if no other item is active"),
      '#default_value' => $this->configuration['highlight_default'] ?? $defaults->get('highlight_default'),
      '#weight' => '0',
    ];
    $form['ignore_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ignore selector'),
      '#description' => $this->t('Elements that you do not want to be used to generate the table of contents'),
      '#default_value' => $this->configuration['ignore_selector'] ?? $defaults->get('ignore_selector'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['scroll_history'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Scroll history'),
      '#description' => $this->t('Adds a hash to the page URL, to maintain history, when scrolling to a table of contents item'),
      '#default_value' => $this->configuration['scroll_history'] ?? $defaults->get('scroll_history'),
      '#weight' => '0',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['theme'] = $form_state->getValue('theme');
    $this->configuration['container'] = $form_state->getValue('container');
    $this->configuration['selectors'] = $form_state->getValue('selectors');
    $this->configuration['show_and_hide'] = $form_state->getValue('show_and_hide');
    $this->configuration['show_effect'] = $form_state->getValue('show_effect');
    $this->configuration['show_effect_speed'] = $form_state->getValue('show_effect_speed');
    $this->configuration['hide_effect'] = $form_state->getValue('hide_effect');
    $this->configuration['hide_effect_speed'] = $form_state->getValue('hide_effect_speed');
    $this->configuration['smooth_scroll'] = $form_state->getValue('smooth_scroll');
    $this->configuration['smooth_scroll_speed'] = $form_state->getValue('smooth_scroll_speed');
    $this->configuration['scroll_to'] = $form_state->getValue('scroll_to');
    $this->configuration['show_and_hide_on_scroll'] = $form_state->getValue('show_and_hide_on_scroll');
    $this->configuration['highlight_on_scroll'] = $form_state->getValue('highlight_on_scroll');
    $this->configuration['highlight_offset'] = $form_state->getValue('highlight_offset');
    $this->configuration['extend_page'] = $form_state->getValue('extend_page');
    $this->configuration['extend_page_offset'] = $form_state->getValue('extend_page_offset');
    $this->configuration['history'] = $form_state->getValue('history');
    $this->configuration['hash_generator'] = $form_state->getValue('hash_generator');
    $this->configuration['highlight_default'] = $form_state->getValue('highlight_default');
    $this->configuration['ignore_selector'] = $form_state->getValue('ignore_selector');
    $this->configuration['scroll_history'] = $form_state->getValue('scroll_history');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'container__tocify__table_of_contents',
      '#attached' => ['library' => ['tocify/tocify']],
      '#attributes' => [
        'class' => ['table-of-contents'],
        'data-context' => $this->configuration['container'],
        'data-theme' => $this->configuration['theme'],
        'data-selectors' => $this->configuration['selectors'],
        'data-show-and-hide' => $this->configuration['show_and_hide'],
        'data-show-effect' => $this->configuration['show_effect'],
        'data-show-effect-speed' => $this->configuration['show_effect_speed'],
        'data-hide-effect' => $this->configuration['hide_effect'],
        'data-hide-effect-speed' => $this->configuration['hide_effect_speed'],
        'data-smooth-scroll' => $this->configuration['smooth_scroll'],
        'data-smooth-scroll-speed' => $this->configuration['smooth_scroll_speed'],
        'data-scroll-to' => (string) $this->configuration['scroll_to'],
        'data-show-and-hide-on-scroll' => $this->configuration['show_and_hide_on_scroll'],
        'data-highlight-on-scroll' => $this->configuration['highlight_on_scroll'],
        'data-highlight-offset' => (string) $this->configuration['highlight_offset'],
        'data-extend-page' => $this->configuration['extend_page'],
        'data-extend-page-offset' => (string) $this->configuration['extend_page_offset'],
        'data-history' => $this->configuration['history'],
        'data-hash-generator' => $this->configuration['hash_generator'],
        'data-highlight-default' => $this->configuration['highlight_default'],
        'data-ignore-selector' => $this->configuration['ignore_selector'],
        'data-scroll-history' => $this->configuration['scroll_history'],
      ],
    ];
    return $build;
  }

}
