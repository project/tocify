/**
 * @file
 * Defines Javascript behaviors for the tocify module.
 */

(($, Drupal) => {
  Drupal.behaviors.nodeDetailsSummaries = {
    attach: function (context) {
      once('tocify-init', '.table-of-contents', context).forEach((element) => {
        $(element).tocify({
          theme: $(element).data('theme'),
          context: $(element).data('context'),
          selectors: $(element).data('selectors'),
          showAndHide: $(element).data('show-and-hide'),
          showEffect: $(element).data('show-effect'),
          showEffectSpeed: $(element).data('show-effect-speed'),
          hideEffect: $(element).data('hide-effect'),
          hideEffectSpeed: $(element).data('hide-effect-speed'),
          smoothScroll: $(element).data('smooth-scroll'),
          scrollTo: $(element).data('scroll-to'),
          showAndHideOnScroll: $(element).data('show-and-hide-on-scroll'),
          highlightOnScroll: $(element).data('highlight-on-scroll'),
          highlightOffset: $(element).data('highlight-offset'),
          extendPage: $(element).data('extend-page'),
          extendPageOffset: $(element).data('extend-page-offset'),
          history: $(element).data('history'),
          hashGenerator: $(element).data('hash-generator'),
          highlightDefault: $(element).data('highlight-default'),
          ignoreSelector: $(element).data('ignore-selector'),
          scrollHistory: $(element).data('scroll-history')
        });
      });
    }
  };
})(jQuery, Drupal);
